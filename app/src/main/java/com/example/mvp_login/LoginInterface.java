package com.example.mvp_login;

public interface LoginInterface {

    // định nghĩa những cái hàm mà để nó giao tiếp vs thằng view
    void loginSuccess();
    void loginError();

}
