package com.example.mvp_login;

public class LoginPresenter {

    private LoginInterface mLoginInterface;

    public LoginPresenter(LoginInterface mLoginInterface) {
        this.mLoginInterface = mLoginInterface;
    }

    public void login(User user){
        if (user.isValidEmail() && user.isValidPassword()) {
            this.mLoginInterface.loginSuccess();
        }else{
            this.mLoginInterface.loginError();
        }


    }
}
